from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.cancer_query_view, name='query'),
	url(r'^results', views.cancer_results_view, name='query_results'),
	url(r'^download', views.cancer_download_view, name='download_results'),
]
