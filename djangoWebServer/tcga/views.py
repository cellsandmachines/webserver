from django.shortcuts import render
from django.http import HttpResponse, Http404
# from django.core.servers.base

from mrna_mapping.models import Batch

import pickle
import csv

# Create your views here.
data_dir = '/home/jinyi/NAR/'

def cancer_query_view(req):
    return render(req, "cancer_types.html")


def cancer_results_view(req):

    if req.method == "POST":
        cancer_type = str(req.POST["cancer_type"])
        mRNA_name = str(req.POST["cancer_mrna_match"])

        mRNA_name_list = []

        # if a file is uploaded
        if "file_upload" in req.FILES:
            batch_file = Batch(batch_file=req.FILES["file_upload"])
            batch_file.save()
            mRNA_name_list += batch_file.read_file()
            # Clean up
            batch_file.delete_file()

        if mRNA_name:
            mRNA_name_list.append(mRNA_name)

        mRNA_name_list = [str(s) for s in mRNA_name_list]

        filter_mNRA = False
        if mRNA_name_list:
            filter_mNRA = True

        # Determine the file path and load the data
        pickle_file_path = data_dir + cancer_type + '/' + cancer_type + '.p'
        mRNA_list, cluster_list, _ = pickle.load(open(pickle_file_path, 'rb'))

        group_list = []
        group_mRNA_names = {}

        for name in mRNA_name_list:    
            if name in mRNA_list:
                index = mRNA_list.index(name)
                group_id = cluster_list[index]

                if group_id not in group_list:
                    group_list.append(group_id)
                    group_mRNA_names[group_id] = [name]
                else:
                    group_mRNA_names[group_id].append(name)

        # Sort based on group id
        group_list.sort()
        group_mRNA_names_list = [group_mRNA_names[group_id] for group_id in group_list]
        data = zip(group_list, group_mRNA_names_list)

        context = {"data": data, "cancer_type": cancer_type, "filter": filter_mNRA}

        return render(req, "cancer_results.html", context)

    raise Http404()


def cancer_download_view(req):
    if req.method == "POST":
        cancer_type = str(req.POST["data_type"])
        download_type = str(req.POST["data_name"])

        group_id = str(req.POST["data_search"])

        if download_type == "all":

            zip_file_path = data_dir + cancer_type + '/' + cancer_type + '.zip'

            with open(zip_file_path, 'rb') as f:
                zip_file = f.read()

            response = HttpResponse(zip_file, content_type='application/force-download')
            response['Content-Disposition'] = 'attachment; filename="{}.zip"'.format(cancer_type)

            return response

        elif download_type == "mRNA":
            group_id = int(group_id)

            # Load mRNA data
            pickle_file_path = data_dir + cancer_type + '/' + cancer_type + '.p'
            _, _, groups = pickle.load(open(pickle_file_path, 'rb'))

            # Prepare response
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="{}-{}.mRNA.csv"'.format(cancer_type, group_id)

            writer = csv.writer(response)
            writer.writerow(['Official Gene Symbol'])
            for gene in groups[group_id]:
                writer.writerow([gene])

            return response

        elif download_type == "elastic":
            csv_file_path = "{}{}/{}_csv/{}.{}.csv".format(data_dir, cancer_type, cancer_type, cancer_type, group_id)

            with open(csv_file_path, 'r') as f:
                elastic_net_data = f.read()

            response = HttpResponse(elastic_net_data, content_type='image/csv')
            response['Content-Disposition'] = 'attachment; filename="{}-{}.csv"'.format(cancer_type, group_id)

            return response
    
        elif download_type == "heatmap":
            image_path = "{}{}/{}_figures/{}.{}.png".format(data_dir, cancer_type, cancer_type, cancer_type, group_id)

            with open(image_path, "rb") as f:
                image = f.read()
            
            response = HttpResponse(image, content_type='image/png')
            response['Content-Disposition'] = 'attachment; filename="{}-{}.png"'.format(cancer_type, group_id)

            return response


    raise Http404()
