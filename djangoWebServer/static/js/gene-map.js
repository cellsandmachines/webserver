function getGeneData(map_id, search_mRNA, search_miRNA) {
    return new Promise(function(fulfill, reject) {
        var gene_map = $("#gene-map-" + map_id);
        var queryType = gene_map.attr("data-type");
        var dataSize = gene_map.attr("data-size");

        var data = {};
        data["query_type"] = queryType;
        data["data_size"] = dataSize;
        data["search_mRNA"] = search_mRNA;
        data["search_miRNA"] = search_miRNA;

        $.get("/api", data, function(res) {
            res.forEach(function(specimen) {
                var target = specimen["target_range"];
                specimen["target_range"] = processTargetLocs(target);
            });

            fulfill(res);
        });
    });
}

function processTargetLocs(target) {
    var targetLocation = [];
    var breakPoint = target.indexOf("-");

    targetLocation.push(parseInt(target.substring(0, breakPoint)));
    targetLocation.push(parseInt(target.substring(breakPoint + 1)));

    return targetLocation;
}

function getTargetLocs(data) {
    return data.map(function(specimen) {
        return specimen["target_range"];
    });
}

function getFlatTargetLocs(data) {
    var targets = getTargetLocs(data);
    return targets.reduce(function(a, b) {
        return a.concat(b);
    }, []);
}

function drawRuler(canvas, width, startX, startY, min, max, map_id) {
    var top = 0;
    var markerVal = 0;
    var numRows = Math.ceil(max / width);
    var rowWidth = width;

    for (var i = 0; i < numRows; i++) {
        top = startY + (100 * i);

        var rulerBase = new createjs.Shape();
        canvas.addChild(rulerBase);
        rulerBase.graphics.setStrokeStyle(3);
        rulerBase.graphics.beginStroke("black");
        rulerBase.graphics.moveTo(startX, top);
        rulerBase.graphics.lineTo(startX + width, top);
        rulerBase.graphics.endStroke();

        for (var k = startX; k < rowWidth; k += 100) {
            var rulerLabel = new createjs.Text(markerVal.toString(), "12px Lato", "black");
            rulerLabel.x = k;
            rulerLabel.y = top - 15;
            canvas.addChild(rulerLabel);

            var rulerMark = new createjs.Shape();
            canvas.addChild(rulerBase);
            rulerMark.graphics.setStrokeStyle(3);
            rulerMark.graphics.beginStroke("black");
            rulerMark.graphics.moveTo(k, top - 2);
            rulerMark.graphics.lineTo(k, top + 11);
            rulerMark.graphics.endStroke();
            canvas.addChild(rulerMark);

            markerVal += 100;
        }
    }
    // $("#gene-map").attr({ height: top + 100 });
    $("#gene-map-" + map_id).attr({ height: top + 100 });
}

function drawGenes(data, canvas, width, startX, startY, map_id) {
    data.forEach(function(target) {
        var targetRange = target["target_range"];
        var top = startY;
        var left = targetRange[0] + startX;
        var range = targetRange[1] - targetRange[0] + 1;
        while (left > width) {
            left -= width;
            top += 100;
        }

        var gene = new createjs.Shape();
        gene.graphics.beginFill("#8154D1");
        gene.graphics.drawRect(left, top, range, 30);
        gene.graphics.endFill();
        gene.addEventListener("mouseover", function(e) {
            updateGeneInfo(target, map_id);
        });
        gene.addEventListener("mouseout", function(e) {
            resetGeneInfo(map_id);
        });
        canvas.addChild(gene);
        top += 50;
    });
}

// handle overlapping target regions
// handle gene hover -> display contextual data
function drawGeneMap(map_id, search_mRNA, search_miRNA) {
    var rawWidth = $($(".container")[0]).width();
    $("#gene-map-" + map_id).attr({ width: rawWidth });

    getGeneData(map_id, search_mRNA, search_miRNA).then(function(data) {
        // var canvas = new createjs.Stage("gene-map");
        var canvas = new createjs.Stage("gene-map-" + map_id);
        var width = Math.ceil( (rawWidth - 100) / 100 ) * 100;

        var rulerMarks = [];
        var flat = getFlatTargetLocs(data);
        var rawMin = Math.min.apply(null, flat) - 100;
        var min = Math.ceil( rawMin / 100 ) * 100;
        var rawMax = Math.max.apply(null, flat) + 100;
        var max = Math.ceil( rawMax / 100 ) * 100;
        var startX = (rawWidth - width) / 2;

        if (min < 0) min = 0;

        drawRuler(canvas, width, startX, 15, min, max, map_id);
        drawGenes(data, canvas, width, startX, 50, map_id);

        canvas.enableMouseOver(20);
        canvas.update();
    });
}

function updateGeneInfo(target, map_id) {
    // $("#gene-name").html(target.miRNA + " <span id='gene-range'>[ " + target.target_range[0] + " - " + target.target_range[1] + " ]</span>");
    $("#gene-name-" + map_id).html(target.miRNA + " <span id='gene-range'>[ " + target.target_range[0] + " - " + target.target_range[1] + " ]</span>");
}

function resetGeneInfo(map_id) {
    // $("#gene-name").html("Hover or select a gene segment to see more details.");
    $("#gene-name-" + map_id).html("Hover or select a gene segment to see more details.");
}

function highlightSegments(segments) {
    var styles = {};

    segments.forEach(function(segment) {
        var start = segment[0];
        var end = segment[1];

        for (var i = start; i <= end; i++) {
            styles[i] = { textBackgroundColor: "orange" }
        }
    });

    return styles;
}
