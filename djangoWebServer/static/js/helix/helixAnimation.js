var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75, (window.innerWidth / 4) / window.innerHeight, 0.1, 1000);

var renderer = Detector.webgl? new THREE.WebGLRenderer( { antialias: true, alpha: true } ): new THREE.CanvasRenderer();

var lightPurple = 0xB39DDB; // 673AB7
var white = 0xFFFFFF; // B39DDB
renderer.setSize(window.innerWidth / 4, window.innerHeight);
document.getElementById("helix").appendChild(renderer.domElement);

camera.position.z = 50;

var tubeGeometry = new THREE.CylinderGeometry(0.3,0.3,6,32);
var ballGeometry = new THREE.SphereGeometry(0.8,32,32);
var purpleMaterial = new THREE.MeshBasicMaterial( { color: lightPurple } );
var whiteMaterial = new THREE.MeshBasicMaterial( { color: white } );

var dna = new THREE.Object3D();
var holder = new THREE.Object3D();


for (var i = 0; i <= 40; i++) {
    var purpleTube = new THREE.Mesh(tubeGeometry, purpleMaterial);
    purpleTube.rotation.z = 90 * Math.PI/180;
    purpleTube.position.x = -3;

    var purpleTube2 = new THREE.Mesh(tubeGeometry, purpleMaterial );
    purpleTube2.rotation.z = 90 * Math.PI/180;
    purpleTube2.position.x = 3;


    var ballRight = new THREE.Mesh( ballGeometry, whiteMaterial );
    ballRight.position.x = 6;

    var ballLeft = new THREE.Mesh( ballGeometry, whiteMaterial );
    ballLeft.position.x = -6;

    var row = new THREE.Object3D();
    row.add(purpleTube);
    row.add(purpleTube2);
    row.add(ballRight);
    row.add(ballLeft);

    row.position.y = i*2;
    row.rotation.y = 30*i * Math.PI/180;

    dna.add(row);
};

dna.position.y = -40;

scene.add(dna);

dna.position.y = -40;
holder.add(dna)
scene.add(holder);

var render = function () {
    requestAnimationFrame(render);

    // 	holder.rotation.x += 0.01;
    holder.rotation.y += 0.005;
    renderer.render(scene, camera);
}

render();
