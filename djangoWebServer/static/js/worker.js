onmessage = function(e) {
	importScripts("/static/js/bacon.min.js");
	var search = e.data.search;
	var results = [];

	self.Bacon.fromArray(e.data.data).filter(function(name) {
		var n = search.length;
		var i = 0;

		if (n > name.length) {
			return false;
		}

		var nickName = name.substring(0, n).toLowerCase();

		if (search.toLowerCase().localeCompare(nickName) == 0) {
			return true;
		} else {
			return false;
		}
	}).take(5).onValue(function(suggestion) {
		results.push(suggestion);
	});

	postMessage(results);
}
