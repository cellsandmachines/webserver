$(function() {
    workers = [];
    disableUnusedField();

    // Bind ajax call to download button
    $(".btn-download").click(function(){

        // Create a form
        var downloadForm = jQuery("<form>", {
            "action": "/download",
            "method": "POST",
        });

        downloadForm.append(jQuery("<input>", {
            "name": "csrfmiddlewaretoken",
            "value": $("#csrf_token_div").children().val(),
            "type": "hidden"
        }));

        downloadForm.append(jQuery("<input>", {
            "name": "data_size",
            "value": $(this).attr("data-size"),
            "type": "hidden"
        }));

        downloadForm.append(jQuery("<input>", {
            "name": "query_type",
            "value": $(this).attr("data-type"),
            "type": "hidden"
        }));

        downloadForm.append(jQuery("<input>", {
            "name": "search_term",
            "value": $(this).attr("data-search"),
            "type": "hidden"
        }));

        // Submit the form
        downloadForm.submit();

        // Clean up
        downloadForm.remove();
    });


    // Draw gene-map for each group
    $(".gene-map").each(function(){
        var queryType = $(this).attr("data-type");
        var map_id = $(this).attr("data-search"); 

        var search_mRNA = "";
        var search_miRNA = "";
        if (queryType == "mrna"){
            search_mRNA = map_id;
            search_miRNA = "";
        }
        else{
            search_mRNA = $(this).attr("data-search-mRNA");
            search_miRNA = $(this).attr("data_search-miRNA");
        }

        // Draw the visualization map
        drawGeneMap(map_id, search_mRNA, search_miRNA);
    });
    
    $("#mrna_query").autocomplete({
        source: suggestmRNA
    });

    $("#mirna_query").autocomplete({
        source: suggestmiRNA
    });

    function disableUnusedField() {
        updateDisabling();

        $("#query_type").change(function(e) {
            updateDisabling();
        });

        function updateDisabling() {
            var queryType = $("#query_type").val();

            if (queryType === "mrna") {
                $("#mrna_query").prop("disabled", false);
                $("#mirna_query").prop("disabled", true);
            } else {
                $("#mirna_query").prop("disabled", false);
                $("#mrna_query").prop("disabled", true);
            }
        }
    }

    function suggest(req, res, data) {
        killWorkers(workers);
        workers = [];

        var worker = new Worker("/static/js/worker.js");
        workers.push(worker);

        worker.onmessage = function(e) {
            res(e.data);
        }

        worker.postMessage({
            search: req.term,
            data:   data
        });
    }

    function killWorkers(workers) {
        for (var i = 0; i < workers.length; i++) {
            workers[i].terminate();
        }
    }

    function suggestmRNA(req, res) {
        suggest(req, res, mRNA_names);
    }

    function suggestmiRNA(req, res) {
        suggest(req, res, miRNA_names);
    }
});
