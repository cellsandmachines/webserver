from django.core.management.base import BaseCommand
from mrna_mapping.models import mRNA, miRNA, mRNA_miRNA_Main_Data

import csv
import MySQLdb


class Command(BaseCommand):

	def handle(self, *args, **options):


		"""
		Init mRNA name and seq
		"""
		mRNA_list = [x['name'] for x in mRNA.objects.values('name')]
		with open('dict.csv', 'rb') as f:
			reader = csv.reader(f)
			for line in reader:
				mrna, seq = line
				if mrna not in mRNA_list:
					mRNA.objects.create(name=mrna, seq=seq)

		print("mRNA init successfully")


		# """
		# Init miRNA name and seq
		# """
		miRNA_list = [x['name'] for x in miRNA.objects.values('name')]
		with open('mature.fa', 'r') as f:
			lines = f.readlines()
			for i in range(0, len(lines), 2):
				mirna = lines[i].strip('>').split()[0]
				seq = lines[i + 1].strip()

				if mirna not in miRNA_list:
					miRNA.objects.create(name=mirna, seq=seq)

		print("miRNA init successfully")


		"""
		Init main mapping data from mysql db
		"""
		host = 'localhost'
		user = ''
		password = ''
		db = ''

		with open('my.cnf', 'r') as f:
			lines = f.readlines()

		for line in lines:
			if 'user' in line:
				user = line.strip().split('=')[1]
			if 'password' in line or 'passwd' in line:
				password = line.strip().split('=')[1]
			if 'db' in line or 'database' in line:
				db = line.strip().split('=')[1]


		database = MySQLdb.connect(host=host,
			user=user,
			passwd=password,
			db=db)

		cur = database.cursor()
		sql = 'select * from data;'

		cur.execute(sql)

		mRNA_list = [x['mRNA_name'] for x in mRNA_miRNA_Main_Data.objects.values('mRNA_name')]
		miRNA_list = [x['miRNA_name'] for x in mRNA_miRNA_Main_Data.objects.values('miRNA_name')]
		target_location_list = [x['target_location'] for x in mRNA_miRNA_Main_Data.objects.values('target_location')]

		for row in cur.fetchall():
			mRNA_name, miRNA_name, target_location, ignore, true_label, score = row

			if mRNA_name not in mRNA_list or miRNA_name not in miRNA_name or target_location not in target_location_list:
				mRNA_miRNA_Main_Data.objects.create(mRNA_name=mRNA_name, 
												miRNA_name=miRNA_name,
												target_location=target_location,
												ignore_this=ignore,
												true_label=int(true_label),
												score=score)

		database.close()


		print("Main data init successfully")








