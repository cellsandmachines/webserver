"""
WSGI config for djangoWebServer project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.insert(0, '/home/jinyi/webserver/djangoWebServer/')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "djangoWebServer.settings")

application = get_wsgi_application()
