import csv

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, Http404
from .forms import MRNAForm
from django.utils import encoding
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib import messages
# Create your views here.

from models import mRNA, miRNA, mRNA_miRNA_Main_Data, Batch, UnfoundQuery

def index(req):
    return render(req, 'index.html')

def contact_view(req):
    return render(req, 'contact.html')

def download_view(req):
    if req.method == "POST":
        input_type = req.POST["query_type"]

        data_size = req.POST["data_size"]
        search_term = req.POST["search_term"]

        if input_type == "mrna":
            data_list = mRNA_miRNA_Main_Data.objects.filter(mRNA_name=search_term).order_by("-score")[:data_size]
        else:
            data_list = mRNA_miRNA_Main_Data.objects.filter(miRNA_name=search_term).order_by("-score")[:data_size]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="{}.csv"'.format(search_term)

        writer = csv.writer(response)
        writer.writerow(['ID', 'mRNA', 'miRNA', 'Target Location', 'True Label', 'Score'])
        for i, row in enumerate(data_list):
            writer.writerow([i + 1, row.mRNA_name, row.miRNA_name, row.target_location, row.true_label, row.score])

        return response

    raise Http404()

def processQuery(req):
    if req.method == 'POST':
        input_type = req.POST["query_type"]
        k = req.POST["num_results"]
        
        search_term_list = []

        # if a file is uploaded
        if "file_upload" in req.FILES:
            batch_file = Batch(batch_file=req.FILES["file_upload"])
            batch_file.save()
            search_term_list += batch_file.read_file()
            # Clean up
            batch_file.delete_file()

        if input_type == 'mirna' and req.POST["mirna_query"]:
            search_term = req.POST["mirna_query"]
            search_term_list.append(search_term)
        elif input_type == 'mrna' and req.POST["mrna_query"]:
            search_term = req.POST["mrna_query"]
            search_term_list.append(search_term)
        elif not search_term_list:
            messages.error(req, 'Please specify the gene or microRNA you would like to query.')
            return promptQuery(req)

        list_data_list, num_of_data_list = mRNA_miRNA_Main_Data.mapping(input_type, search_term_list, k)


        # If some of the query cannot be found in the database 
        # --> Empty querySet --> save the query to database --> Generate a link as well
        mapping_results = []
        
        for i in range(len(search_term_list)):
            if num_of_data_list[i] == 0:
                # Save the unfounded query to database
                unfounded_query = UnfoundQuery.objects.create(query=search_term_list[i], query_type=input_type)
                unfounded_query.results_link = "http://" + req.META['HTTP_HOST'] + "/retrieve/" + str(unfounded_query.id)
                unfounded_query.save()

                t = search_term_list[i], unfounded_query, num_of_data_list[i]
            else:
                t = search_term_list[i], list_data_list[i], num_of_data_list[i]

            mapping_results.append(t)

        context = {
            "query_type":  input_type,
            "mapping_results": mapping_results
        }

        return render(req, "results.html", context)

    raise Http404()

    # Landing page to welcome users, link to query_page and format_page
    # Format page to explain how to format uploadable genomic data
    # Query page to make DB queries
        # Make one consolidated form request
        # Render results to seperate paginated results_page?

def promptQuery(req):
    form = MRNAForm()

    mRNA_data = list(set(mRNA_miRNA_Main_Data.objects.values_list("mRNA_name", flat=True)))
    miRNA_data = list(set(mRNA_miRNA_Main_Data.objects.values_list("miRNA_name", flat=True)))

    context = {
        "mRNA_names":  list(map(lambda str: encoding.force_str(str), mRNA_data)),
        "miRNA_names": list(map(lambda str: encoding.force_str(str), miRNA_data))
    }

    return render(req, "query.html", context)

def primitiveAPI(req):
    input_type = req.GET["query_type"]

    data_size = req.GET["data_size"]
    search_mRNA = req.GET["search_mRNA"]
    search_miRNA = req.GET["search_miRNA"]

    if search_miRNA:
        data_list = mRNA_miRNA_Main_Data.objects.filter(mRNA_name=search_mRNA, miRNA_name=search_miRNA).order_by("-score")[:data_size]
    else:
        data_list = mRNA_miRNA_Main_Data.objects.filter(mRNA_name=search_mRNA).order_by("-score")[:data_size]

    context = []

    for specimen in data_list:
        data = {}
        data["mRNA"] = specimen.mRNA_name
        data["miRNA"] = specimen.miRNA_name
        data["target_range"] = specimen.target_location

        context.append(data)

    return HttpResponse(DjangoJSONEncoder().encode(context), content_type='application/json')


def retrieve_view(req, retrieve_id):

    query = UnfoundQuery.objects.get(id=retrieve_id)

    context = {'query': query}

    if not query.processed:
        context['processed'] = False
    else:
        # The query has been processed --> it should be presented in the current database
        context['processed'] = True
        
    return render(req, 'retrieve.html', context)





