from __future__ import unicode_literals

import os

from django.db import models
from djangoWebServer.settings import MEDIA_ROOT

# Create your models here.
class mRNA(models.Model):
    name = models.CharField(max_length=30)
    seq = models.TextField()

    def __unicode__(self):
        return self.name

class miRNA(models.Model):
    name = models.CharField(max_length=30)
    seq = models.TextField()

    def __unicode__(self):
        return self.name

class mRNA_miRNA_Main_Data(models.Model):
    mRNA_name = models.CharField(max_length=30)
    miRNA_name = models.CharField(max_length=30)
    target_location = models.CharField(max_length=30)
    ignore_this = models.CharField(max_length=30)
    true_label = models.IntegerField()
    score = models.DecimalField(decimal_places=6, max_digits=10)

    def __unicode__(self):
        return str(self.mRNA_name) + ' ' + str(self.miRNA_name)

    @staticmethod
    def mapping(input_type, search_term_list, k):
        """
        """
        list_data_list = []
        num_of_data_list = []

        for search_term in search_term_list:
            data_list = []

            if input_type == 'mirna':
                # data_list = [mRNA_miRNA_Main_Data.objects.filter(mRNA_name=search_term).order_by("-score")[:k]]
                tmp_dict = {}
                tmp_query_set = mRNA_miRNA_Main_Data.objects.filter(miRNA_name=search_term).order_by("-score")[:k]

                for obj in tmp_query_set:
                    if obj.mRNA_name not in tmp_dict:
                        tmp_dict[obj.mRNA_name] = [obj]
                    else:
                        tmp_dict[obj.mRNA_name].append(obj)

                for mRNA_name in tmp_dict.keys():
                    data_list.append((mRNA_name, tmp_dict[mRNA_name]))

                num_of_data_list.append(len(tmp_query_set))

            elif input_type == 'mrna':
                data_list = mRNA_miRNA_Main_Data.objects.filter(mRNA_name=search_term).order_by("-score")[:k]
                num_of_data_list.append(len(data_list))
            
            list_data_list.append(data_list)

        return list_data_list, num_of_data_list


class Batch(models.Model):
    batch_file = models.FileField(upload_to='batch_uploads/')

    def __unicode__(self):
        """
        Return the relative path
        """
        return self.batch_file.name

    def read_file(self):
        """
        Return the list of lines
        """
        with open(self.batch_file.path, 'r') as f:
            lines = filter(lambda x: x != '', [x.strip() for x in f.readlines()])
        return lines

    def delete_file(self):
        """
        Remove the file from DB and hard disk
        """
        if os.path.isfile(self.batch_file.path):
            os.remove(self.batch_file.path)
        self.delete()

class UnfoundQuery(models.Model):
    query = models.CharField(max_length=30)
    results_link = models.CharField(max_length=200)
    query_type = models.CharField(max_length=20)
    processed = models.BooleanField(default=False)



    