from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^contact', views.contact_view, name='contact_view'),
    url(r'^download', views.download_view, name='download_view'),
    url(r'^retrieve/(?P<retrieve_id>[0-9]+)', views.retrieve_view, name='retrieve_view'),
    url(r'^search', views.promptQuery, name='test_query'),
    url(r'^results', views.processQuery, name='queried_index'),
    url(r'^api', views.primitiveAPI, name='primitive_api')
]
