from django import forms

query_choices = [("mi", "miRNA"), ("rg", "Regulated Gene/Gene Fragment"), ("ig", "Intergenic Region")]

class MRNAForm(forms.Form):
    query_input = forms.ChoiceField(label = "Given your input of",
                                    label_suffix = "",
                                    choices = query_choices)

    desired_output = forms.ChoiceField(label = "you want",
                                       label_suffix = "",
                                       choices = query_choices)

    batch_request = forms.ChoiceField(label = "Is this a batch query?",
								      label_suffix = "",
                                      choices = [("Y", "Yes"), ("N", "No")],
    								  widget = forms.RadioSelect)

    query_params = forms.ChoiceField(label = "mRNA",
                                     choices = [("tst", "test RNA"), ("abc", "abc RNA")])
