from __future__ import unicode_literals

from django.apps import AppConfig


class MrnaMappingConfig(AppConfig):
    name = 'mrna_mapping'
