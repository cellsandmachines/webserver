from django.contrib import admin

# Register your models here.
from models import mRNA, miRNA, mRNA_miRNA_Main_Data, Batch, UnfoundQuery

class mRNAAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'seq')

class miRNAAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'seq')

class mRNA_miRNA_Main_DataAdmin(admin.ModelAdmin):
    list_display = ('id', 'mRNA_name', 'miRNA_name', 'target_location', 'true_label', 'score')

class BatchAdmin(admin.ModelAdmin):
    list_display = ('id', '__unicode__')

class UnfoundQueryAdmin(admin.ModelAdmin):
    list_display = ('id', 'query', 'results_link', 'query_type', 'processed')

admin.site.register(mRNA, mRNAAdmin)
admin.site.register(miRNA, miRNAAdmin)
admin.site.register(mRNA_miRNA_Main_Data, mRNA_miRNA_Main_DataAdmin)
admin.site.register(Batch, BatchAdmin)
admin.site.register(UnfoundQuery, UnfoundQueryAdmin)
