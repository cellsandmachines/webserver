# README #

### What is this repository for? ###

The system provides a web interface for users to achieve two things, namely, (1) submit queries to match either input microRNAs (miRNAs) with the genes or input genes to do the reverse mapping, (2) submit a cancer type to visualize its miRNA-mRNA interactions through a list of heat maps. The software is based on our algorithm that integrated sequence and thermodynamic features and that can determine both canonical (exact sequence complementarity) and non-canonical matches [1, 2, 3]. 
The web server with the visualization is available at: http://mira.ecn.purdue.edu:8000/

[1] Ghoshal, Asish, Ananth Grama, Saurabh Bagchi, and Somali Chaterji. "An ensemble svm model for the accurate prediction of non-canonical microrna targets." In Proceedings of the 6th ACM Conference on Bioinformatics, Computational Biology and Health Informatics (BCB), pp. 403-412. ACM, 2015. [** Best paper award **]

[2] Ghoshal, Asish, Raghavendran Shankar, Saurabh Bagchi, Ananth Grama, and Somali Chaterji. "MicroRNA target prediction using thermodynamic and sequence curves." BMC genomics 16, no. 1 (2015): pp. 1--21.

[3] Ghoshal, Asish, Jinyi Zhang, Michael Roth, Kevin Xia, Ananth Grama, and Somali Chaterji. "A Distributed Classifier for MicroRNA Target Prediction with Validation through TCGA Expression Data." IEEE/ACM Transactions on Computational Biology and Bioinformatics (2018).

Further details: 
(1)
The system uses a database of the microRNA (miRNA) and mRNA mappings that was created by running an algorithm from ACM BCB 15's best paper: 
http://dl.acm.org/citation.cfm?id=2808761. 
The algorithm is called *Avishkar*, from the Sanskrit for discovery. When the user submits the query, this will be looked up in the database and the results returned through a web frontend. The system currently supports the following types of queries:
1. A single miRNA (hsa-let-7i-5p, etc ...) to be mapped to all the mRNAs that it regulates
2. A single mRNA (NM_000071, etc … ) to be reverse mapped to all the miRNAs that regulate it
3. Batch queries of type 1, i.e., with multiple miRNAs
4. Batch queries of type 2, i.e., with multiple genes (mRNAs)

The lookup result is a table and each row consists of mRNA name, miRNA name, target location, true label, and the probability value for the mapping. The top-*k* results are returned and the value *k* is configurable by the user. The option of downloading the results in csv format is also available. The output also shows the RNA sequences of mRNAs and miRNAs. The front-end interface highlights the targeted locations in the RNA sequences for each regulated miRNA-mRNA pair. 
If a query is not available in the database, the system connects to its backend, applies the previously trained models, and generates predictions for the submitted query. The backend (*Avishkar*) is a distributed SVM implementation on Apache-SPARK for the prediction of non-canonical miRNA targets [CITE: “Ghoshal, A., Grama, A., Bagchi, S., & Chaterji, S. (2015, September). An ensemble SVM model for the accurate prediction of non-canonical MicroRNA targets. In Proceedings of the 6th ACM Conference on Bioinformatics, Computational Biology and Health Informatics (pp. 403-412). ACM.”]. This nonlinear model is an ensemble of miRNA family-specific models. If the miRNA belongs to none of the families, the system uses a linear SVM model, instead. In addition, for the benefit of assigning importance scores to different features (classifier interpretability), the outputs of the linear SVM are always returned. The user can take her pick of the linear or nonlinear results. Periodically, the new data points are used to do retraining of the models. 



### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Who do I talk to? ###

Somali Chaterji (Purdue University) or Jinyi Zhang (Columbia University)

schaterji@purdue.edu or jz2776@columbia.edu